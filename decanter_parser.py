# -*- coding: utf-8 -*-
import time
import json
from bs4 import BeautifulSoup
from selenium import webdriver


class DecanterParser(object):

    def __init__(self, url, path_to_chromedriver='./lib/chromedriver'):
        self.url = url
        self.wine_description_list = []

        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        self.browser = webdriver.Chrome(executable_path=path_to_chromedriver, options=options)

    def parse(self):
        self.browser.get(self.url)
        page = self.browser.page_source
        soup = BeautifulSoup(page, 'html5lib')
        page_counter = 0
        while True:
            button_next_action = self.browser.find_elements_by_xpath(
                '//*[@id="root"]/div[2]/div[5]/div[2]/div[2]/div[1]/ul/li[4]')[0]
            buttons = soup.findAll('a', {'class': 'dwwa-page-link'})
            pages_qty = len(buttons) - 2
            if page_counter >= pages_qty:
                break

            table = soup.find('tbody').contents
            if len(table) > 0:
                for row in table:
                    self.wine_description_list.append(self._get_wine_description(row.contents))

            button_next_action.click()
            page = self.browser.page_source
            soup = BeautifulSoup(page, 'html5lib')
            page_counter += 1
            time.sleep(5)

    def _get_wine_description(self, row):
        wine_description = {}
        wine_description["Producer"] = row[0].text
        wine_description["Wine name"] = row[1].text
        wine_description["Award"] = row[2].next.attrs['title']
        wine_description["Score"] = row[3].text
        wine_description["Country"] = row[4].text
        wine_description["Region"] = row[5].text
        wine_description["Sub-Region"] = row[6].text
        wine_description["Vintage"] = row[7].text
        wine_description["Color"] = row[8].text
        return wine_description

    def get_json(self):
        json_str = []
        self.parse()
        try:
            json_str = json.dumps(self.wine_description_list)
        finally:
            pass
        return json_str


if __name__ == '__main__':
    parser = DecanterParser("https://awards.decanter.com/DWWA/2020/search/wines?_ga=2.113192644.1768569384.1617272692-35891752.1616434398&award=Best%20in%20Show&competitionType=DWWA")
    result = parser.get_json()
    print(result)
